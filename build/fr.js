/*! fr */
var fr = {
    userModels : {},
    registerModel : function(name, model) {
       fr.userModels[name] = model;
    }
};
;fr.helpers = {
    // это чтобы не перетиралась ссылка на объект this.mainViewTransformation
    objectUpdate : function (object, properties) {
        for (var prop in properties) {
            if (properties.hasOwnProperty(prop) && object.hasOwnProperty(prop)){
                object[prop] = properties[prop];
            }
        }
    },

    // adds unexistent properties to target from source and sets them the values from source
    objectFill : function (target, source) {
        for (var prop in source) {
            if (!target.hasOwnProperty(prop) && source.hasOwnProperty(prop)){
                target[prop] = source[prop];
            }
        }
        return target;
    },

    objectExtend : function (target, source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
        return target;
    },

    objectClone : function (source) {
        var new_object = {};
        for (var a in source) {
            if (typeof(source[a]) == 'object') {
                new_object[a] = fr.helpers.objectClone(source[a])
            } else {
                new_object[a] = source[a];
            }
        }
        return new_object;
    },

    update : function(o1, o2) {
        if (typeof o1 == 'object' && typeof o2 == 'object') {
            fr.helpers.objectUpdate (o1, o2)
        } else {
            o1 = o2;
        }
    },

    compare : function (o1, o2) {
        if (typeof o1 == 'object' && typeof o2 == 'object') {
            // here the check is not full (one sided only for props in o1)
            for (var prop in o1) {
                if (o1.hasOwnProperty(prop) && o2.hasOwnProperty(prop) && !fr.helpers.compare(o1[prop], o2[prop])) {
                    return false;
                }
            }
            return true;
        } else {
            return o1 === o2;
        }

    },
}
;fr.mixins = {
    'eventSource' : {
//        events : {},
        initMixin : function() {
            this.events = {};
        },
        on : function(eventName, callback, context){
            if (this.events[eventName] === undefined){
                this.events[eventName] = [];
            }
            this.events[eventName].push({
                'callback' : callback,
                'context' : context
            });
        },
        trigger : function(eventName, params){
            if (this.events.hasOwnProperty(eventName)) {
                var event = this.events[eventName];
                for (listener in event) {
                    event[listener].callback.call(event[listener].context, params);
                }
            }
        },
        off : function() {

        }
    },

    'eventListener' : {
        // смысл написанного здесь - утерян
//        initMixin : function(){
//            // if object has model attribute, attach events from it to given handlers
//            if (this.model && this.model.events) {
//                for (var eventName in this.eventsCallbacks){
//                    if (this.eventsCallbacks.hasOwnProperty(eventName)){
//                        this.model.on(eventName, this[this.eventsCallbacks[eventName]], this)
//                    }
//                }
//            }
//        },
        listen : function(source, eventName, callback, context){
            source.on(eventName, callback, context || this)
        },
        listenAll : function(sourceModel, eventName, callback, context){
            var oldInit = sourceModel.prototype.init
            sourceModel.prototype.init = function() {
                oldInit.apply(this, arguments);
                this.on(eventName, callback, context || this);
            }
        },
        unbind :function(object){

        }
    }
}
;
fr.core = (function(){
//    var inherit = (function(){
//        function F() {}
//        return function (child, parent) {
//            F.prototype = parent.prototype;
//            child.prototype = new F;
//            child.prototype.constructor = child;
////            child.superproto = parent.prototype;
//            return child;
//        };
//    })();

    var inherit = function(child, parent) {
        child.prototype = Object.create(parent.prototype || {});
        child.prototype.constructor = child;
    }

    //наследование
    var _extend = function(class_attributes){
        // proto - родительский конструктор
        var proto = typeof (this) == 'function' ? this : function(){};
        var out = function() {
            //вызов родительского конструктора (рекурсивно вызывает out)
            proto.apply(this, arguments);


//            производит initMixin на всех mixinах в контексте объекта
            if (this.mixins) {
                for (var i = 0, len = this.mixins.length; i < len; i++) {
                    if (fr.mixins.hasOwnProperty(this.mixins[i])) {
                        var newMixin = fr.mixins[this.mixins[i]];
                        if(typeof(newMixin.initMixin) === 'function') {
                            newMixin.initMixin.apply(this, arguments);
                        }
                    }
                }
                //чтобы 2 раза не инит
                this.mixins = false;
            }

            // все аргументы конструктора уходят в цепочку инитов
            // вызов init ( по всей цепочке прототипов из-за рекурсии в out)
            // arguments.callee.prototype нужен, чтобы получить ссылку на объект, в контексте которого был вызван new
            // (при рекурсии подменяется)

//            if(Object.getPrototypeOf(this).hasOwnProperty('init')) {
//                console.log(arguments.callee.prototype, Object.getPrototypeOf(this),
//                    fr.helpers.compare(arguments.callee.prototype, Object.getPrototypeOf(this)));
//                Object.getPrototypeOf(this).init.apply(this, arguments);
//            }
            if(arguments.callee.prototype.hasOwnProperty('init')) {
                arguments.callee.prototype.init.apply(this, arguments);
            }

            //ссылка на родительский прототип
            this.parent = proto.prototype;


        };

        inherit(out, this);

        //adding all mixins to out prototype
        if (class_attributes.mixins !== undefined){
            for (var i= 0, len = class_attributes.mixins.length; i < len; i++) {
                if (fr.mixins.hasOwnProperty(class_attributes.mixins[i])) {
                    var newMixin = fr.mixins[class_attributes.mixins[i]]
                    fr.helpers.objectExtend( out.prototype, newMixin );
                }
            }
        }

        // добавление к прототипу out свойств и методов из class_attributes
        fr.helpers.objectExtend( out.prototype, class_attributes );
        // чтобы от получившегося класса можно было наследоваться
        out.extend = _extend;
        return out;
    };

    return {
        //добавление класса в core фреймворк
        // context добавлен, чтобы можно было унаследоваться от Array или String
        add: function(name, class_attributes, context) {
            fr[name] = _extend.call((context || this), class_attributes);
        },
        getUniqueId : function () {
            if (this.curr === undefined) {
                this.curr = 0
            } else {
                this.curr++
            }
            return this.curr
        }
    };
})();
;fr.core.add('ObjectCollection', {
    mixins : ['eventSource','eventListener'],
    init : function(model, name) {
//        console.log('collection created', url);
//        if (model) {
//        }
        this.name = name;
    },
//    fetch : function() {
//        console.log('fetching collection from ' + this.url);
//    },
    push : function(obj) {
        this.parent.push.call(this, obj);
        this.trigger('change');
        this.listen(obj, 'destroy', this._remove)
    },
    // служебный метод
    _remove : function(obj) {
        this.splice(this.indexOf(obj),1);
        this.trigger('change')
    },
    remove : function(obj) {
        obj.destroy();
    },
    // this is for the case of dealyed deleting
    checkDead : function() {
        var len = this.length, changed = false;
        for (var i=0; i < len; i++ ) {
            var current_object = this[i];
            if (current_object.marked_as_dead) { // удаление ссылки на уничтоженный
                current_object.trigger('destroy');
                this.splice(i,1);
                i--;
                len--;
                changed = true
            }
        }
        if (changed) {
            this.trigger('change');
        }
    },
}, Array);
;fr.core.add('ObjectModel', {
//    modelName : 'BaseModel',
    mixins : ['eventSource'],
    init : function(attributes) {
        this.attributes = fr.helpers.objectClone(this.attributes);
        // присваиваем значения тем атрибутам, которые перданы в инит
        for (var a in attributes){
            if (this.attributes.hasOwnProperty(a)){
                this.attributes[a] = attributes[a];
            }
        }
//        this.attributes = attributes;
//        fr.helpers.objectExtend( this, attributes );
        this.id = fr.core.getUniqueId();
        this.applySchema(this.schema);

        if (attributes.hasOwnProperty('modelName')) {
            this.modelName = attributes.modelName;
            this.url = this.modelName;
        }
    },

    applySchema : function(schema) {
        for (relation in schema) {
            this[relation] = fr.Relations.applyRelation(this, schema[relation])
        }
    },

    destroy : function() {
        this.marked_as_dead = true;
        this.trigger('destroy', this);
// TODO:        this.clearEventListeners();
    },
    'get' : function(attribute) {
        if (this.attributes.hasOwnProperty(attribute)) {
            return this.attributes[attribute];
        } else if (attribute in this) { // если что-то не задано для каждого объекта,
                                        // возможно, оно задано на модели общим для всех объектов
            return this[attribute];
        }

    },
    'set' : function(attribute, value){
        this.attributes[attribute] = value;
        this.trigger('change');
    },
    // Обновляем атрибут модели, если он объект - частично (без уничтожения его)
    update : function(attribute, newValues) {
//        console.log(attribute, this.get(attribute))
        fr.helpers.update(this.get(attribute), newValues);
        this.trigger('change');
    },

//    fetch : function() {
////        console.log('fetching ' + this.modelName  + ' from ' + this.url);
//        this.trigger('fetch');
//    },

    toJSON : function() {
        return JSON.stringify(this.attributes);
    }

});
;// абстрактный вид объекта


fr.core.add('ObjectView', {
    mixins : ['eventSource', 'eventListener'],
    rendered : false,
    init : function(model) {
        this.model = model;
        this.listen(this.model, 'destroy', this.destroy, this);
    },

    // задание свойств модели для наблюдения за ними
    observeProperties : function(properties){
        this.observedProperties = {};
        for (var i= 0; i < properties.length; i++) {
//            console.log(properties[i])
            this.observedProperties[properties[i]] = fr.helpers.objectClone(this.model.get(properties[i]));
        }
//        console.log('!!!', this.observedProperties)
    },

    // вью перерисовывает свой элемент при изменении заданных свойств модели
    // возвращает bool - нарисован ли объект
    update : function() {
        if (!this.rendered){
            this.render();
            this.rendered = true;
            return true;
        }
        if ('observedProperties' in this){
            var properties = this.observedProperties,
                changed = false;
            for (var attrName in properties) {

//                console.log(fr.helpers.compare(properties[attrName], this.model[attrName]))
                if (!fr.helpers.compare(properties[attrName], this.model.get(attrName))) {
                    changed = true;
//                    console.log(properties[attrName])
                    fr.helpers.update(properties[attrName], this.model.get(attrName))
                }
            }
            if (changed) {
                this.render();
                return true;
            }
        }
        return false;
    },
    destroy: function() {
        this.marked_as_dead = true;
//        this.trigger('destroy');
    }

});


;fr.CanvasObjectView = fr.ObjectView.extend({


    init : function (model, transform) {
        this.model = model;
        // позиция на канвасе
        this.position = transform(this.model.get('position'));
//        if (this.model.hasOwnProperty('view_attribu   tes')) {
//            fr.helpers.objectExtend( this, model.view_attributes);
////        delete model.view_attributes;
//        }
        this.initOwnCanvas();
    },

    initOwnCanvas : function() {
        this.ownCanvas = window.document.createElement('canvas');
        this.ownCanvasContext = this.ownCanvas.getContext('2d');
    },
//
//    getTopLeftPosition : function(){
//        return {x: this.position.x - this.canvasCenter.x, y: this.position.y - this.canvasCenter.y};
//    },

    //установление позиции на канвасе
    updatePosition : function(transform){
        var new_position = transform(this.model.get('position'));
        if (!fr.helpers.compare(this.position, new_position)) {
            this.position = new_position;
            return true; // поменялась позиция
        }
        return false; // не поменялась
    },

    putOnCanvas :  function(canvas_context, scale) {
        canvas_context.drawImage(this.ownCanvas,
            this.position.x - this.canvasCenter.x * scale,
            this.position.y - this.canvasCenter.y * scale,
            this.ownCanvas.width * scale,
            this.ownCanvas.height * scale
        );
//        this.trigger('step', [this.position, this.model.get('color')]);
    }
});

;fr.DOMObjectView = fr.ObjectView.extend({
    template_dummy : function(model){
        var docFrag = document.createDocumentFragment();
//        for (var i in model) {
//            console.log(model)
//            console.log(i, model.hasOwnProperty(i))
//            var el = document.createElement('div');
//            el.textContent = JSON.stringify(model[i]);
//            docFrag.appendChild(el);
//        }
        var el = document.createElement('div');
        el.textContent = JSON.stringify(model.toJSON());
        docFrag.appendChild(el);
        return docFrag;

    },


    init : function(model, el, template) {
        this.el = document.createElement(el || 'div');
        this.template = template || this.template_dummy;
        this.render();
    },

    getObservableElement :  function() {
        return this.el;
    },

    render : function() {
//        var new_docFrag = this.template(this.model);
//        if (this.el.parentNode) {
//            this.el.parentNode.replaceChild(this.el, new_docFrag);
//        } else {
//            this.el = new_docFrag;
//        }
        // of course this is bad
        // we need to update only changed parts of DOM
        this.el.textContent = '';
//        while(this.el.firstChild){
//            this.el.removeChild(this.el.firstChild);
//        }


        this.el.appendChild(this.template(this.model));
    },


});


;
// нужно бы и radius с position добавлять на атрибуты для CircleView или нет?
// не надо же мне их синхронизировать с сервером в самом деле
// возможно, стоит у атрибутов ставить флажок (синхронизируеимый/нет)
fr.CircleModel = fr.ObjectModel.extend({
//    modelName : 'CircleModel',
    shape : 'Circle',

    radius : 1,
    position: {x:0, y:0},

    // обязательно дл всех Shapes
    pointBelongs: function(x,y){
        var position = this.get('position'),
            radius = this.get('radius');
        diffx = position.x - x;
        diffy = position.y - y;
        if (radius * radius > diffx * diffx + diffy * diffy){
            return true;
        }
        return false;
    }
});
;fr.CircleView = fr.CanvasObjectView.extend({
    init:function(){
        // а как он может быть defined?
        if (this.radius === undefined){
            this.radius = this.model.get('radius') || 1;
        }
        this.resetOwnCanvas();
        this.render();
    },
    findCanvasCenter : function(){
        this.canvasCenter = {x: this.ownCanvas.width/2|0, y: this.ownCanvas.height/2|0}
    },
    resetOwnCanvas : function() {
//        console.log(this.radius)
        this.ownCanvas.width = 2 * this.radius + 1;
        this.ownCanvas.height = 2 * this.radius + 1;
        this.findCanvasCenter();
    },
    clearOwnCanvas : function() {
        this.ownCanvasContext.clearRect(- this.canvasCenter.x, - this.canvasCenter.y, this.ownCanvas.width, this.ownCanvas.height);
    },

    render : function() {
        // тут просто рисование
        var own_context = this.ownCanvasContext;
        own_context.fillStyle = this.model.get('color');
        own_context.beginPath();
        own_context.arc(this.canvasCenter.x, this.canvasCenter.y , this.radius, 0, Math.PI*2, true);
        own_context.fill();
    },
});
;
;fr.LineModel = fr.ObjectModel.extend({
    shape : 'Line'
});
;fr.core.add('MainModel', {
    objects : (new fr.ObjectCollection()),
//    init : function {
//        this.objects = new fr.Collection();
//        this.objects.push({})
//    },

    addObject: function(model) {
        this.objects.push(model);
    },

    getModelsOfType: function(modelName) {
        return this.objects.filter(function(obj){return obj.modelName == modelName});
    },

    findObjectsByXY: function (x, y) {
        return this.objects.filter(function(obj){return obj.pointBelongs(x, y)});
    },

    start: function(field_size, objects){

    },


});
;// это - вид, содержаший все канвасы, управляет рендером


fr.core.add('MainView', {
    init:function(){
        this.views = {}
    },
    addView : function(view){
        this.views[view.name] = view;
        return view;
    },

    startWaiting : function(){
        for (var viewName in this.views) {
            this.views[viewName].render();
            this.views.startWaiting();
        }
    },

    startAnimation: function(){
        this.previous_step_time = Date.now();
        requestAnimationFrame(this.animate(this));
    },

    animate: function(_this){
        return function(timestamp){
            _this.renderFrame(timestamp - _this.previous_step_time);
            _this.previous_step_time = timestamp;
            requestAnimationFrame(_this.animate(_this));
        }
    },

    renderFrame: function(timediff){ // step of the game
        for (var viewName in this.views) {
            this.views[viewName].render(timediff)
        }
    }
});
;// TODO: вынести общие с канвасовым вью части в общего родителя (типа areaView)
fr.core.add('DOMView', {

    // надо как-то передавать отступы в канвас, а оттуда во все лэеры
    alignments : ['left', 'right', 'top', 'bottom'],

    init: function(el, domId, width, height, position, zIndex) {

        this.objectViews = [];
        this.modelTypes = {};
        this.name = domId;

        this.el = document.createElement(el);
        this.el.id = domId;

        document.body.appendChild(this.el);
        if (width) {
            this.el.style.width = width + 'px';
        }
        if (height) {
            this.el.style.height = height + 'px';
        }
        this.el.style.position = "absolute";
        this.el.style.zIndex = zIndex || 0;
        this.position = position || {left : 0, top : 0};
        for (var alignment in this.alignments) {
            if (this.position[this.alignments[alignment]] !== undefined) {
                this.el.style[this.alignments[alignment]] = this.position[this.alignments[alignment]];
            }
        }
        this.zIndex = zIndex || 0;
        this.drawn = false;
    },

    startWaitnig : function(){
//        this.object.on('change')
    },

    render : function() {
//        console.log(newContent)
        this._checkObjectViews();
        if (!this.drawn) {
            this.drawn = true;
            this.el.textContent = '';
            this.el.appendChild(this._renderObjectViews());
        }
    },
    // object view is just dom subviews
    addObjectView : function(model) {
        if (this.modelTypes.hasOwnProperty( model.modelName )) {
            var newViewObject = new this.modelTypes[ model.modelName ]( model );
            this.objectViews.push( newViewObject );
        }
    },

    // добавление, перерисовка вью измененившихся моделей и удаление связанных с удаленной моделью вьюх
    _checkObjectViews : function() {
        if (!this.drawn) return;
        var objectViews = this.objectViews,
            len = objectViews.length;
        for (var i = 0; i < len; i++) {
            // удаление
            if (objectViews[i].marked_as_dead) {
                objectViews.splice(i, 1);
                i--;
                len--;
                this.drawn = false;
            } else {
                var oldel = objectViews[i].el;
                if (objectViews[i].update()) {
                    // добавление
                    if (!this.el.contains(oldel)) {
                        this.el.appendChild(objectViews[i].el);
                    // перерисовка
                    } else {
                        this.el.replaceChild(objectViews[i].el, oldel);
                    }
                }
            }
        }
    },
//
//    // удалим дочерний
//    _remove : function(view){
//        console.log(obj, this.indexOf(obj))
//        this.splice(this.indexOf(obj)+1,1);
//        this.trigger('change')
//    },

    // returns documentFragment
    _renderObjectViews : function () {
        var docFragment = document.createDocumentFragment();
        for (var i = 0; i < this.objectViews.length; i++) {
            docFragment.appendChild(this.objectViews[i].el);
        }
        return docFragment;
    },

    addModelObserve: function(modelName, objectView) {
        this.modelTypes[modelName] = objectView;
    },

    getObservableElement :  function() {
        return this.el;
    },

});


;// Может стоит замутить с большим количеством слоев?
// чтобы перерисовывались только при изменении принадлежащих им моделей
// соответственно,


fr.core.add('canvasLayer', {

    drawn : false,
    static : false,

    init: function(domId, width, height, zIndex){
        this.modelTypes = {};
        this.objectViews = [];
        var canvas = document.createElement('canvas');
        canvas.id = domId;
        document.body.appendChild(canvas);
        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);
        canvas.style.zIndex = zIndex;
        this.canvas = canvas;
        this.canvasContext = canvas.getContext('2d');
    },

    // transform - преобразование координат, передающееся с CanvasView
    render : function(transform, scale) {
//        if (this.objectViews && (!this.static || !this.drawn)) {
        this._checkObjectViews(transform);
        if (!this.drawn) {
            this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this._renderObjectViews(transform, scale);
            this.drawn = true;
        }
    },

    // modifies this.drawn and this.objectViews
    _checkObjectViews : function(transform) {
        if (!this.drawn) return;
        var objectViews = this.objectViews;
        var len = objectViews.length;
        for (var i=0; i < len; i++ ) {
            if (objectViews[i].marked_as_dead) {
                objectViews.splice(i, 1);
                i--;
                len--;
                this.drawn = false;
            } else {
                // вычисляем позицию с учетом смещения канваса
                if (objectViews[i].updatePosition(transform) || objectViews[i].update()) {
                    this.drawn = false;
                }
            }
        }
    },
    _renderObjectViews: function (transform, scale) {
        var objectViews = this.objectViews;
        var len = objectViews.length;
        for (var i=0; i < len; i++ ) {
            objectViews[i].putOnCanvas(this.canvasContext, scale);
        }

    },

// можно дописать, чтобы трейсы ресайзились все-таки
    transformContent : function(transformFunction, transformation){
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this._renderObjectViews(transformFunction, transformation.scale);
        this.drawn = false;
    }
});





;// Может стоит замутить с большим количеством слоев?
// чтобы перерисовывались только при изменении принадлежащих им моделей
// соответственно,

// это канвасовый вид, может быть несколько слоев, каждый из которых - канвас
// в целом - канвас
// суть - наличие слоев


fr.core.add('CanvasView', {

    // надо как-то передавать отступы в канвас, а оттуда во все лэеры
    alignments : ['left', 'right', 'top', 'bottom'],
    transformation : {offsetX: 0, offsetY: 0, scale: 1.0},
    init: function(name, width, height, position, zIndex){

        this.layers = {};

        this.name = name;
        this.width = width;
        this.height = height;
        this.position = position || {left : 0, top : 0};
        this.zIndex = zIndex || 0;
        this._addLayer(name, undefined, this.zIndex);
    },

    //sets objectModel with @modelName to be displayed on @layerName by @objectView
    addModelObserve: function(modelName, objectView, layerName){
        this.layers[layerName || this.name].modelTypes[modelName] = objectView;
    },
    // может добавить image кроме color
    addBackground : function(layer, color){
        var bg = this._addLayer('background', layer, this.zIndex-1);
        bg.static = true;
        if (color !== undefined){
            bg.canvas.style.backgroundColor = color;
        }
    },

    // adds view of object to all layers it belongs to
    addObjectView : function(model) {
        var layers = this.layers;
        for (var layerName in layers) {
            if (layers[ layerName ].modelTypes.hasOwnProperty( model.modelName )) {
                var newViewObject = new layers[ layerName ].modelTypes[ model.modelName ]( model, this.makeTransformFunction());
                layers[layerName].objectViews.push( newViewObject );
            }
        }
    },

    // @layer is layerClass, which can be user-defined
    _addLayer : function(name, layer, zIndex) {
        var newName = name == this.name ? name : this.name + name;
        layer = new (layer || fr.canvasLayer)(newName, this.width, this.height, zIndex || 0);
        this._placeLayer(layer);
        return this.layers[name] = layer;
    },

    _placeLayer : function(layer){
        var canvas = layer.canvas
        canvas.style.position = "absolute";
        for (var alignment in this.alignments) {
            if (this.position[this.alignments[alignment]] !== undefined) {
                canvas.style[this.alignments[alignment]] = this.position[this.alignments[alignment]];
            }
        }
    },

    render : function() {
        for (var name in this.layers) {
            this.layers[name].render(this.makeTransformFunction(), this.transformation.scale);
        }
    },

    // identical transformation functions
    makeTransformFunction : function() {
        if (this.transfromFunction === undefined){
            this.transfromFunction = function(pos){return fr.helpers.objectClone(pos)};
        }
        return this.transfromFunction;
    },

    getObservableElement :  function() {
        return this.layers[this.name].canvas;
    }

});


;fr.CanvasCameraView = fr.CanvasView.extend({
    // типа какое-то дефолтное говно
    cameraParams : {
        scaleDelta : 1.25,
        moveDelta : 100,
        targetWidth : 800,
        targetHeight : 600,
        minScale : 0.1,
        maxScale : 2.0
    },
    init: function(name, width, height, position, zIndex, transformation, cameraParams){
        fr.helpers.objectUpdate(this.cameraParams, cameraParams);
        if(typeof (transformation) !== 'undefined'){
            this.transformation = transformation;
        } else {
            this.transformation = {offsetX: 0, offsetY: 0, scale: 1.0}
        }
    },

//    render : function() {
//        for (var name in this.layers) {
//            this.layers[name].render(this.makeTransformFunction(), this.transformation.scale);
//        }
//    },

    makeTransformFunction : function(){
        var _this = this;
        return function(position){
            return _this.transformCoords.call(_this, position)
        }
    },

    transformCoords : function(modelPosition) {
        // пожалуй, создание объекта для позиции на каждом шаге не оптимально
        var position = {};
        position.x = (modelPosition.x + this.transformation.offsetX)*this.transformation.scale;
        position.y = (modelPosition.y + this.transformation.offsetY)*this.transformation.scale;
        return position;
    },

    // transforms coords from view to model ones
    backwardTransformCoords : function(viewPosition) {
        var position = {};
        position.x = viewPosition.x / this.transformation.scale - this.transformation.offsetX;
        position.y = viewPosition.y / this.transformation.scale - this.transformation.offsetY;
        return position;
    },

    _getViewTransformationByChange: function(transformationChange){
        var transformation = fr.helpers.objectClone(this.transformation);
        var new_scale = transformation.scale * transformationChange.scale;
        transformation.offsetX += - (this.width / transformation.scale - this.width / new_scale) / 2 + transformationChange.offsetX;
        transformation.offsetY += - (this.height / transformation.scale - this.height / new_scale) / 2 + transformationChange.offsetY;
        transformation.scale = new_scale;
        return transformation;

    },

    _setViewTransformation :function(transformation){
        fr.helpers.objectUpdate(this.transformation, transformation);
        for (var layer in this.layers){
            if( this.layers[layer].static ) {
                this.layers[layer].drawn = false;
            }
        }
    },

    transformView: function(transformationChange){
//        console.log(this._getViewTransformationByChange(transformationChange))
        this._setViewTransformation(this._getViewTransformationByChange(transformationChange));
        return this.transformation;
    },

    // Дальше - управление Camera

    // проверка допустимости трансформации
    _testTransformation: function (transformationChange) {
        // Это очень плохо!
        var targetWidth = this.cameraParams.targetWidth,
            targetHeight = this.cameraParams.targetHeight,
            min_scale = this.cameraParams.minScale,
            max_scale = this.cameraParams.maxScale;

        // передача ссылок на transformationChange и и саму трансформацию слегка нетривиальная, надо бы исправить
        var new_tr = this._getViewTransformationByChange(transformationChange);
        if (new_tr.scale < min_scale || new_tr.scale > max_scale) {
            return false;
        }
        var curr_win_width = this.width / new_tr.scale,
            curr_win_height = this.height / new_tr.scale;

        if (curr_win_width >targetWidth || curr_win_height > targetHeight ){
            var widthExcessRatio = curr_win_width /targetWidth,
                heightExcessRatio = curr_win_height / targetHeight;
            transformationChange.scale *= Math.max(widthExcessRatio, heightExcessRatio);
        }

        // Ладно, сначала будем добиваться корректности программы, потом оптимальности

        var out_x_right = new_tr.offsetX - curr_win_width + targetWidth,
            out_y_bottom = new_tr.offsetY - curr_win_height + targetHeight;


//      выход за границы поля
        if (new_tr.offsetX > 0 || new_tr.offsetY > 0 || out_x_right < 0 || out_y_bottom < 0) {
            // если выход за границу экрана вышел по причине уменьшения масштаба
            if (transformationChange.scale > 1) {
                if (new_tr.offsetX > 0) {
                    transformationChange.offsetX = - new_tr.offsetX;
                }
                //выход вправо - надо сместить
                if (out_x_right < 0) {
                    transformationChange.offsetX -= out_x_right;
                }
                if (new_tr.offsetY > 0) {
                    transformationChange.offsetY = - new_tr.offsetY;
                }
                if (out_y_bottom < 0) {
                    transformationChange.offsetY -= out_y_bottom;
                }
            } else { // выход по причине смещения
                if (new_tr.offsetX > 0) {
                    transformationChange.offsetX = - this.transformation.offsetX;
                } else if (out_x_right < 0) {
                    transformationChange.offsetX = - this.transformation.offsetX -
                        (targetWidth - curr_win_width);
                }
                if (new_tr.offsetY > 0) {
                    transformationChange.offsetY = - this.transformation.offsetY;
                } else if (out_y_bottom < 0) {
                    transformationChange.offsetY = - this.transformation.offsetY-
                        ( targetHeight - curr_win_height);
                }
            }
        }
        return true;
    },

    changeScaleAroundPoint : function (scalingDirection, pointX, pointY) {
        var prevScale = this.transformation.scale;
        this.changeScale(scalingDirection);
//        if (wheelDelta < 0 ) {
//            var scaleRel = prevScale /  this.mainViewTransformation.scale;
        var cameraCenterX = this.width / 2,
            cameraCenterY = this.height / 2;
        var scale_change = this.transformation.scale/ prevScale;
        this.moveCameraByDxDy(
            - (pointX - cameraCenterX) * (scale_change - 1) / this.transformation.scale,
            - (pointY - cameraCenterY) * (scale_change - 1) / this.transformation.scale
        );
//        }
    },

    changeScale: function (direction) {
        var scaleDelta = this.cameraParams.scaleDelta;
        var transformationChange = {
            offsetX: 0,
            offsetY: 0,
            scale: (direction === '+') ? 1 / scaleDelta : scaleDelta
        };
        if (this._testTransformation(transformationChange)) {
//            fr.helpers.objectUpdate(this.transformation, this.transformView(transformationChange));
            this.transformView(transformationChange)
        }
    },

    moveCameraToDirection: function (direction) {
        var moveDistance = this.cameraParams.moveDelta;
        var mapping = {
            'left' : [moveDistance, 0],
            'right' : [-moveDistance, 0],
            'up' : [0, moveDistance],
            'down' : [0, -moveDistance]
        };
        this.moveCameraByDxDy(mapping[direction][0], mapping[direction][1]);
    },

    moveCameraByDxDy: function (dx, dy) {
        var transformationChange = {
            offsetX: dx,
            offsetY: dy,
            scale: 1
        };
        if (this._testTransformation(transformationChange)) {
//            fr.helpers.objectUpdate(this.transformation, this.transformView(transformationChange));
            this.transformView(transformationChange)
        }
    },

    moveCameraTo: function(x, y) {
        var cameraCenterX = this.transformation.offsetX - this.width / 2 / this.transformation.scale,
            cameraCenterY = this.transformation.offsetY - this.height / 2 / this.transformation.scale;
        var transformationChange = {
            offsetX: -(x + cameraCenterX),
            offsetY: -(y + cameraCenterY),
            scale: 1
        };
        if (this._testTransformation(transformationChange)) {
            this.transformView(transformationChange)
        }
    }

// если двигать окно мышкой
//    testForMouseMoveCamera : function (x, y) {
//        var move_area = 50,
//            move_distance = 10;
//        if (x < move_area) {
//            this.moveCameraByDxDy(move_distance, 0);
//        } else if (x >  window.innerWidth - move_area) {
//            this.moveCameraByDxDy(-move_distance, 0);
//        }
//        if (y < move_area) {
//            this.moveCameraByDxDy(0, move_distance);
//        } else if (y > window.innerHeight - move_area) {
//            this.moveCameraByDxDy(0, -move_distance);
//        }
//    }

});

;fr.core.add('Resources', {
    data : [],
    init : function(){

    },
    loadedCount : 0,
    processFunctions : {
        'image' : function(resource, callback, context) {
            var image = new Image();
            image.src = resource.url;
            resource.output = image;
            image.onload = function(){
                callback.call(context)
            }
        },
        'json' : function(resource, callback, context) {
            var oReq = new XMLHttpRequest();
            oReq.onreadystatechange = function(){
                if (oReq.readyState == 4) {
                    if (oReq.status == 200) {
                        resource.output = JSON.parse(oReq.responseText);
                        callback.call(context);
                    } else {
                        alert('load failed');
                    }
                }
            };
            oReq.open("GET", resource.url, true);
            oReq.send()
        }
    },

    // @resourcesList = [ {
    //     name : name,
    //     url : url,
    //     type : type
    // }, ...]
    addList : function(resourcesList){
        for (var i=0; i < resourcesList.length; i++) {
            this.data.push(resourcesList[i]);
        }
    },

    'get' : function(name) {
        for (var i=0; i < this.data.length; i++) {
            if (this.data[i].name === name) {
                return this.data[i].output;
            }
        }
        return false;
    },

    loadAll : function(callback, context){

        var i, resourcesCount = this.data.length,
            localCallback = function() {
                this.loadedCount++;
                if (this.loadedCount === resourcesCount){
                    callback.call(context)
                }
            };
        for (i = 0; i < resourcesCount; i++) {
            if (this.processFunctions.hasOwnProperty(this.data[i].type)) {
                this.processFunctions[this.data[i].type](this.data[i], localCallback, this);
            }
        }
    }
})
;fr.Relations = {
    applyRelation : function(ownerObject, relation) {
        if (ownerObject[relation]) {
            console.log('duplicate property in schema and object definition');
        } else {
            return this[relation.type](ownerObject, relation.model, relation.url);
        }
    },
    hasMany : function (ownerObject, relatedModelName, url) {
//        console.log(this)
        var collection = new fr.ObjectCollection(fr.userModels[relatedModelName], url || relatedModelName + 's');
        // TODO: Нормальное удаление ссылок на объекты при удалении добавленных в листенере
//        console.log(ownerObject)
        collection.listen(ownerObject, 'fetch', collection.fetch, collection);
        return collection

    },

    hasOne : function (ownerObject, relatedModelName, url) {
        var model = new fr.userModels[relatedModelName];
        model.listen(ownerObject, 'fetch', collection.fetch, collection);
        return model;
    }

};
;// Это модуль с игровой логикой (типа главный контроллер)
// https://docs.google.com/drawings/d/1fddot6Xf0iq4Hl2zIR-JgQSmh4zdhvpH9fuop1pxY4w/edit
fr.core.add('MainController', {
//    mixins : ['eventListener'],
    init : function(){
        this.model = new fr.MainModel();
        this.view = new fr.MainView();
    },

    start : function(){

        this.model.start();

        var canvasView = new fr.CanvasView('defaultCanvas', window.innerWidth, window.innerHeight);
        this.view.addCanvasView(canvasView);
        this.view.start();
    },

    addObjects: function(objects_data) {
        var new_models = [];
        objects_data.forEach(function(object_data) {
            var new_model = new fr.userModels[ object_data.modelName ]( object_data );
            var views = this.view.views;
            for (var viewName in views) {
                views[viewName].addObjectView(new_model);
            }
            new_models.push(new_model);
            this.model.addObject(new_model);
        }, this);
        return new_models;
    },

    addObserver : function(view, event, handler, DOMelement) {
        var _this = this;
        DOMelement = DOMelement || view.getObservableElement()
        DOMelement.addEventListener(
            event,
            function(event){
                handler.call(_this, event, view);
            }
        );
    }
});


//    changeScale: function(dir){
//         var diff = 0.2, scale_factor;
//         /*var tmp_canvas = document.createElement('canvas');
//         var base_width = this.staticCanvas.width;
//         var base_height = this.staticCanvas.height;
//         tmp_canvas.height = base_height;
//         tmp_canvas.width = base_width;
//         var tmp_context = tmp_canvas.getContext('2d');
//         tmp_context.drawImage(this.staticCanvasContext, 0, 0);*/
//         if (dir == '+') {
//             scale_factor = 1+diff;
//         } else {
//             scale_factor = 1-diff;
//         }
//         this.view.staticCanvasContext.scale(scale_factor, scale_factor);
//         this.view.staticCanvasContext.clearRect(0, 0, this.gameFieldSize.width, this.gameFieldSize.height);
//         this.view.drawStatic(this.objects);
//         this.view.activeCanvasContext.scale(scale_factor, scale_factor);
//         this.view.activeCanvasContext.clearRect(0, 0, this.gameFieldSize.width, this.gameFieldSize.height);
//         /*
//         this.staticCanvasContext.clearRect(0, 0, this.activeCanvas.width, this.activeCanvas.height);
//         this.staticCanvasContext.drawImage(tmp_context, 0, 0, base_width*scale_factor, base_height*scale_factor)
//         */
//
//    },
