// Это модуль с игровой логикой (типа главный контроллер)
// https://docs.google.com/drawings/d/1fddot6Xf0iq4Hl2zIR-JgQSmh4zdhvpH9fuop1pxY4w/edit
fr.core.add('MainController', {
//    mixins : ['eventListener'],
    init : function(){
        this.model = new fr.MainModel();
        this.view = new fr.MainView();
    },

    start : function(){

        this.model.start();

        var canvasView = new fr.CanvasView('defaultCanvas', window.innerWidth, window.innerHeight);
        this.view.addCanvasView(canvasView);
        this.view.start();
    },

    addObjects: function(objects_data) {
        var new_models = [];
        objects_data.forEach(function(object_data) {
            var new_model = new fr.userModels[ object_data.modelName ]( object_data );
            var views = this.view.views;
            for (var viewName in views) {
                views[viewName].addObjectView(new_model);
            }
            new_models.push(new_model);
            this.model.addObject(new_model);
        }, this);
        return new_models;
    },

    addObserver : function(view, event, handler, DOMelement) {
        var _this = this;
        DOMelement = DOMelement || view.getObservableElement()
        DOMelement.addEventListener(
            event,
            function(event){
                handler.call(_this, event, view);
            }
        );
    }
});


//    changeScale: function(dir){
//         var diff = 0.2, scale_factor;
//         /*var tmp_canvas = document.createElement('canvas');
//         var base_width = this.staticCanvas.width;
//         var base_height = this.staticCanvas.height;
//         tmp_canvas.height = base_height;
//         tmp_canvas.width = base_width;
//         var tmp_context = tmp_canvas.getContext('2d');
//         tmp_context.drawImage(this.staticCanvasContext, 0, 0);*/
//         if (dir == '+') {
//             scale_factor = 1+diff;
//         } else {
//             scale_factor = 1-diff;
//         }
//         this.view.staticCanvasContext.scale(scale_factor, scale_factor);
//         this.view.staticCanvasContext.clearRect(0, 0, this.gameFieldSize.width, this.gameFieldSize.height);
//         this.view.drawStatic(this.objects);
//         this.view.activeCanvasContext.scale(scale_factor, scale_factor);
//         this.view.activeCanvasContext.clearRect(0, 0, this.gameFieldSize.width, this.gameFieldSize.height);
//         /*
//         this.staticCanvasContext.clearRect(0, 0, this.activeCanvas.width, this.activeCanvas.height);
//         this.staticCanvasContext.drawImage(tmp_context, 0, 0, base_width*scale_factor, base_height*scale_factor)
//         */
//
//    },
