// абстрактный вид объекта


fr.core.add('ObjectView', {
    mixins : ['eventSource', 'eventListener'],
    rendered : false,
    init : function(model) {
        this.model = model;
        this.listen(this.model, 'destroy', this.destroy, this);
    },

    // задание свойств модели для наблюдения за ними
    observeProperties : function(properties){
        this.observedProperties = {};
        for (var i= 0; i < properties.length; i++) {
//            console.log(properties[i])
            this.observedProperties[properties[i]] = fr.helpers.objectClone(this.model.get(properties[i]));
        }
//        console.log('!!!', this.observedProperties)
    },

    // вью перерисовывает свой элемент при изменении заданных свойств модели
    // возвращает bool - нарисован ли объект
    update : function() {
        if (!this.rendered){
            this.render();
            this.rendered = true;
            return true;
        }
        if ('observedProperties' in this){
            var properties = this.observedProperties,
                changed = false;
            for (var attrName in properties) {

//                console.log(fr.helpers.compare(properties[attrName], this.model[attrName]))
                if (!fr.helpers.compare(properties[attrName], this.model.get(attrName))) {
                    changed = true;
//                    console.log(properties[attrName])
                    fr.helpers.update(properties[attrName], this.model.get(attrName))
                }
            }
            if (changed) {
                this.render();
                return true;
            }
        }
        return false;
    },
    destroy: function() {
        this.marked_as_dead = true;
//        this.trigger('destroy');
    }

});

