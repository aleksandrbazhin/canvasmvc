fr.CanvasCameraView = fr.CanvasView.extend({
    // типа какое-то дефолтное говно
    cameraParams : {
        scaleDelta : 1.25,
        moveDelta : 100,
        targetWidth : 800,
        targetHeight : 600,
        minScale : 0.1,
        maxScale : 2.0
    },
    init: function(name, width, height, position, zIndex, transformation, cameraParams){
        fr.helpers.objectUpdate(this.cameraParams, cameraParams);
        if(typeof (transformation) !== 'undefined'){
            this.transformation = transformation;
        } else {
            this.transformation = {offsetX: 0, offsetY: 0, scale: 1.0}
        }
    },

//    render : function() {
//        for (var name in this.layers) {
//            this.layers[name].render(this.makeTransformFunction(), this.transformation.scale);
//        }
//    },

    makeTransformFunction : function(){
        var _this = this;
        return function(position){
            return _this.transformCoords.call(_this, position)
        }
    },

    transformCoords : function(modelPosition) {
        // пожалуй, создание объекта для позиции на каждом шаге не оптимально
        var position = {};
        position.x = (modelPosition.x + this.transformation.offsetX)*this.transformation.scale;
        position.y = (modelPosition.y + this.transformation.offsetY)*this.transformation.scale;
        return position;
    },

    // transforms coords from view to model ones
    backwardTransformCoords : function(viewPosition) {
        var position = {};
        position.x = viewPosition.x / this.transformation.scale - this.transformation.offsetX;
        position.y = viewPosition.y / this.transformation.scale - this.transformation.offsetY;
        return position;
    },

    _getViewTransformationByChange: function(transformationChange){
        var transformation = fr.helpers.objectClone(this.transformation);
        var new_scale = transformation.scale * transformationChange.scale;
        transformation.offsetX += - (this.width / transformation.scale - this.width / new_scale) / 2 + transformationChange.offsetX;
        transformation.offsetY += - (this.height / transformation.scale - this.height / new_scale) / 2 + transformationChange.offsetY;
        transformation.scale = new_scale;
        return transformation;

    },

    _setViewTransformation :function(transformation){
        fr.helpers.objectUpdate(this.transformation, transformation);
        for (var layer in this.layers){
            if( this.layers[layer].static ) {
                this.layers[layer].drawn = false;
            }
        }
    },

    transformView: function(transformationChange){
//        console.log(this._getViewTransformationByChange(transformationChange))
        this._setViewTransformation(this._getViewTransformationByChange(transformationChange));
        return this.transformation;
    },

    // Дальше - управление Camera

    // проверка допустимости трансформации
    _testTransformation: function (transformationChange) {
        // Это очень плохо!
        var targetWidth = this.cameraParams.targetWidth,
            targetHeight = this.cameraParams.targetHeight,
            min_scale = this.cameraParams.minScale,
            max_scale = this.cameraParams.maxScale;

        // передача ссылок на transformationChange и и саму трансформацию слегка нетривиальная, надо бы исправить
        var new_tr = this._getViewTransformationByChange(transformationChange);
        if (new_tr.scale < min_scale || new_tr.scale > max_scale) {
            return false;
        }
        var curr_win_width = this.width / new_tr.scale,
            curr_win_height = this.height / new_tr.scale;

        if (curr_win_width >targetWidth || curr_win_height > targetHeight ){
            var widthExcessRatio = curr_win_width /targetWidth,
                heightExcessRatio = curr_win_height / targetHeight;
            transformationChange.scale *= Math.max(widthExcessRatio, heightExcessRatio);
        }

        // Ладно, сначала будем добиваться корректности программы, потом оптимальности

        var out_x_right = new_tr.offsetX - curr_win_width + targetWidth,
            out_y_bottom = new_tr.offsetY - curr_win_height + targetHeight;


//      выход за границы поля
        if (new_tr.offsetX > 0 || new_tr.offsetY > 0 || out_x_right < 0 || out_y_bottom < 0) {
            // если выход за границу экрана вышел по причине уменьшения масштаба
            if (transformationChange.scale > 1) {
                if (new_tr.offsetX > 0) {
                    transformationChange.offsetX = - new_tr.offsetX;
                }
                //выход вправо - надо сместить
                if (out_x_right < 0) {
                    transformationChange.offsetX -= out_x_right;
                }
                if (new_tr.offsetY > 0) {
                    transformationChange.offsetY = - new_tr.offsetY;
                }
                if (out_y_bottom < 0) {
                    transformationChange.offsetY -= out_y_bottom;
                }
            } else { // выход по причине смещения
                if (new_tr.offsetX > 0) {
                    transformationChange.offsetX = - this.transformation.offsetX;
                } else if (out_x_right < 0) {
                    transformationChange.offsetX = - this.transformation.offsetX -
                        (targetWidth - curr_win_width);
                }
                if (new_tr.offsetY > 0) {
                    transformationChange.offsetY = - this.transformation.offsetY;
                } else if (out_y_bottom < 0) {
                    transformationChange.offsetY = - this.transformation.offsetY-
                        ( targetHeight - curr_win_height);
                }
            }
        }
        return true;
    },

    changeScaleAroundPoint : function (scalingDirection, pointX, pointY) {
        var prevScale = this.transformation.scale;
        this.changeScale(scalingDirection);
//        if (wheelDelta < 0 ) {
//            var scaleRel = prevScale /  this.mainViewTransformation.scale;
        var cameraCenterX = this.width / 2,
            cameraCenterY = this.height / 2;
        var scale_change = this.transformation.scale/ prevScale;
        this.moveCameraByDxDy(
            - (pointX - cameraCenterX) * (scale_change - 1) / this.transformation.scale,
            - (pointY - cameraCenterY) * (scale_change - 1) / this.transformation.scale
        );
//        }
    },

    changeScale: function (direction) {
        var scaleDelta = this.cameraParams.scaleDelta;
        var transformationChange = {
            offsetX: 0,
            offsetY: 0,
            scale: (direction === '+') ? 1 / scaleDelta : scaleDelta
        };
        if (this._testTransformation(transformationChange)) {
//            fr.helpers.objectUpdate(this.transformation, this.transformView(transformationChange));
            this.transformView(transformationChange)
        }
    },

    moveCameraToDirection: function (direction) {
        var moveDistance = this.cameraParams.moveDelta;
        var mapping = {
            'left' : [moveDistance, 0],
            'right' : [-moveDistance, 0],
            'up' : [0, moveDistance],
            'down' : [0, -moveDistance]
        };
        this.moveCameraByDxDy(mapping[direction][0], mapping[direction][1]);
    },

    moveCameraByDxDy: function (dx, dy) {
        var transformationChange = {
            offsetX: dx,
            offsetY: dy,
            scale: 1
        };
        if (this._testTransformation(transformationChange)) {
//            fr.helpers.objectUpdate(this.transformation, this.transformView(transformationChange));
            this.transformView(transformationChange)
        }
    },

    moveCameraTo: function(x, y) {
        var cameraCenterX = this.transformation.offsetX - this.width / 2 / this.transformation.scale,
            cameraCenterY = this.transformation.offsetY - this.height / 2 / this.transformation.scale;
        var transformationChange = {
            offsetX: -(x + cameraCenterX),
            offsetY: -(y + cameraCenterY),
            scale: 1
        };
        if (this._testTransformation(transformationChange)) {
            this.transformView(transformationChange)
        }
    }

// если двигать окно мышкой
//    testForMouseMoveCamera : function (x, y) {
//        var move_area = 50,
//            move_distance = 10;
//        if (x < move_area) {
//            this.moveCameraByDxDy(move_distance, 0);
//        } else if (x >  window.innerWidth - move_area) {
//            this.moveCameraByDxDy(-move_distance, 0);
//        }
//        if (y < move_area) {
//            this.moveCameraByDxDy(0, move_distance);
//        } else if (y > window.innerHeight - move_area) {
//            this.moveCameraByDxDy(0, -move_distance);
//        }
//    }

});
