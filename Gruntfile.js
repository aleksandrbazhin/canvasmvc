module.exports = function(grunt) {
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        concat : {
            options : {
                banner : '/*! <%= pkg.name %> */\n',
                separator : '\n;'
            },
            core : {
                src : '<%= core_files %>',
                dest : 'build/<%= pkg.name %>.js'
            },
            physics : {
                src : '<%= physics_files %>',
                dest : 'build/<%= pkg.name %>-physics.js'
            },
            rest : {
                src : '<%= rest_files %>',
                dest : 'build/<%= pkg.name %>-rest.js'
            },
        },
//        uglify: {
//            options: {
//                banner: '/*! <%= pkg.name %> */\n',
//                separator: '\n;'
//            },
//            build: {
//                src: '<%= core_files, plugin_files %>',
//                dest: 'build/<%= pkg.name %>.min.js'
//            }
//        },
        watch: {
            core : {
                files: ['<%= core_files %>'],
                tasks: [ 'concat:core']
            },
            physics : {
                files: ['<%= physics_files %>'],
                tasks: [ 'concat:physics']
            },
            rest : {
                files: ['<%= rest_files %>'],
                tasks: [ 'concat:rest']
            }
        },
        physics_files : ['src/plugins/Physics/**/*.js'],
        rest_files : ['src/plugins/RESTSync/**/*.js'],
        core_files : ['src/fr.js',
                        'src/helpers.js',
                        'src/Mixins.js',
                        'src/Core.js',
                        'src/ObjectCollection.js',
                        'src/ObjectModel.js',
                        'src/ObjectView.js',
                        'src/ObjectCanvasView.js',
                        'src/ObjectDOMView.js',
                        'src/Shapes/*.js',
                        'src/MainModel.js',
                        'src/MainView.js',
                        'src/DOMView.js',
                        'src/CanvasLayer.js',
                        'src/CanvasView.js',
                        'src/CanvasCameraView.js',
                        'src/Resources.js',
                        'src/Relations.js',
                        'src/MainController.js'],

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat']);

};