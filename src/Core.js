
fr.core = (function(){
//    var inherit = (function(){
//        function F() {}
//        return function (child, parent) {
//            F.prototype = parent.prototype;
//            child.prototype = new F;
//            child.prototype.constructor = child;
////            child.superproto = parent.prototype;
//            return child;
//        };
//    })();

    var inherit = function(child, parent) {
        child.prototype = Object.create(parent.prototype || {});
        child.prototype.constructor = child;
    }

    //наследование
    var _extend = function(class_attributes){
        // proto - родительский конструктор
        var proto = typeof (this) == 'function' ? this : function(){};
        var out = function() {
            //вызов родительского конструктора (рекурсивно вызывает out)
            proto.apply(this, arguments);


//            производит initMixin на всех mixinах в контексте объекта
            if (this.mixins) {
                for (var i = 0, len = this.mixins.length; i < len; i++) {
                    if (fr.mixins.hasOwnProperty(this.mixins[i])) {
                        var newMixin = fr.mixins[this.mixins[i]];
                        if(typeof(newMixin.initMixin) === 'function') {
                            newMixin.initMixin.apply(this, arguments);
                        }
                    }
                }
                //чтобы 2 раза не инит
                this.mixins = false;
            }

            // все аргументы конструктора уходят в цепочку инитов
            // вызов init ( по всей цепочке прототипов из-за рекурсии в out)
            // arguments.callee.prototype нужен, чтобы получить ссылку на объект, в контексте которого был вызван new
            // (при рекурсии подменяется)

//            if(Object.getPrototypeOf(this).hasOwnProperty('init')) {
//                console.log(arguments.callee.prototype, Object.getPrototypeOf(this),
//                    fr.helpers.compare(arguments.callee.prototype, Object.getPrototypeOf(this)));
//                Object.getPrototypeOf(this).init.apply(this, arguments);
//            }
            if(arguments.callee.prototype.hasOwnProperty('init')) {
                arguments.callee.prototype.init.apply(this, arguments);
            }

            //ссылка на родительский прототип
            this.parent = proto.prototype;


        };

        inherit(out, this);

        //adding all mixins to out prototype
        if (class_attributes.mixins !== undefined){
            for (var i= 0, len = class_attributes.mixins.length; i < len; i++) {
                if (fr.mixins.hasOwnProperty(class_attributes.mixins[i])) {
                    var newMixin = fr.mixins[class_attributes.mixins[i]]
                    fr.helpers.objectExtend( out.prototype, newMixin );
                }
            }
        }

        // добавление к прототипу out свойств и методов из class_attributes
        fr.helpers.objectExtend( out.prototype, class_attributes );
        // чтобы от получившегося класса можно было наследоваться
        out.extend = _extend;
        return out;
    };

    return {
        //добавление класса в core фреймворк
        // context добавлен, чтобы можно было унаследоваться от Array или String
        add: function(name, class_attributes, context) {
            fr[name] = _extend.call((context || this), class_attributes);
        },
        getUniqueId : function () {
            if (this.curr === undefined) {
                this.curr = 0
            } else {
                this.curr++
            }
            return this.curr
        }
    };
})();