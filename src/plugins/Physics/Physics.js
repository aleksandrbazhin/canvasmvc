fr.Physics = fr.MainModel.extend({
    G : 0.03, //dummy
//    maxImpactRange : 1000,
    iterationTimeout : 5,
    counter :0,

        /*this.worker = new Worker('/gravity/script/worker.js');
        this.worker.onmessage = function(message){
            _this.drawEverything.apply(_this,[message.data])
        };
        */
    start: function(field_size){
//            setInterval(function(){test +=1; console.log(test);}, 100);
        this.fieldSize = field_size;
        this.startIteration();
    },

// Проблемы: setTimeOut все равно не работает при свернутом браузере
// надо паузить игру при большом интервале и выводить сообщение "пауза"
// т.е сохранять ее состояние в таких случаях


    startIteration: function(){
        this.previous_step_time = Date.now();
        this.intervalHandler = setInterval(this.iterate( this), this.iterationTimeout);
        //window.setTimeout( this.iterate( this), this.iterationTimeout, Date.now() );
    },

    stopIteration: function(){
        clearInterval(this.intervalHandler);
    },

    iterate: function(_this){
        return function(){//timestamp){ // this is a step function
            _this.counter++;
            var timestamp = Date.now();
            _this.computePhysicsState( timestamp - _this.previous_step_time);
            _this.previous_step_time = timestamp;
            //window.setTimeout( _this.iterate(_this), _this.iterationTimeout, Date.now() );
        }
    },

    bounceCircles: function(object_1, object_2){
        var deep, v2ty, v2tx, v2ny, v2nx,
//            v1n_mod, v2n_mod,
            tanx, tany, v1nx, v1ty, v1tx, v1ny;
        if (object_1.collisionStrategy[object_2.modelName] !== 'bounce') {
            var t = object_1;
            object_1 = object_2;
            object_2 = t;
        } // на выходе если 1 не 'bounce' от 2го, то меняем местами

        var rangeX = object_1.position.x - object_2.position.x, rangeY = object_1.position.y - object_2.position.y,
            range_mod = Math.sqrt(rangeX*rangeX + rangeY*rangeY),
            nx = rangeX/range_mod, ny = rangeY/range_mod,
//            average_preserve = (object_1.collisionEnergyPreserve + object_2.collisionEnergyPreserve)/2,
//            friction = (object_1.frictionEnergyPreserve + object_2.frictionEnergyPreserve)/2,
            mass1 = object_1.mass, mass2 = object_2.mass,
            speed1 = object_1.speed, speed2 = object_2.speed;

        deep = ((object_1.radius + object_2.radius) - range_mod); //проваливание
//        console.log(deep)

        tanx = ny;
        tany = -nx;

        // v1n - нормальная, v1t - касательная (трансверсальная) скорости
        v1nx = (speed1.x * nx + speed1.y * ny) * nx;
        v1ny = (speed1.x * nx + speed1.y * ny) * ny;
//        v1n_mod = Math.sqrt(v1nx * v1nx + v1ny * v1ny);
        v1tx = (speed1.x * tanx + speed1.y * tany) * tanx;
        v1ty = (speed1.x * tanx + speed1.y * tany) * tany;

        v2nx = (speed2.x * nx + speed2.y * ny) * nx;
        v2ny = (speed2.x * nx + speed2.y * ny) * ny;
//        v2n_mod = Math.sqrt(v2nx * v2nx + v2ny * v2ny);
        v2tx = (speed2.x * tanx + speed2.y * tany) * tanx;
        v2ty = (speed2.x * tanx + speed2.y * tany) * tany;

//        console.log(this.counter + '  -  ' + 'nx1:' + v1nx + ' nx2:' + v2nx);

//        console.log(this.counter + ' before  :\n' + 'o1:{' + speed1.x + ', ' +  speed1.y +'}\n'
//            + 'o2:{' + speed2.x + ', ' +  speed2.y +'}\n');
        // это если один из объектов (2) не реагирует на второй (1)
        var push = 0.1;
        if (object_2.collisionStrategy[object_1.modelName] !== 'bounce') {
            // расталкивание
            object_1.position.x += (push+deep)*nx;
            object_1.position.y += (push+deep)*ny;

            //изменение скорости
            speed1.x = v1tx + (-v1nx + v2nx);
            speed1.y = v1ty + (-v1ny + v2ny);
        } else {
            var M = mass1 + mass2;
            // расталкивание
            object_1.position.x += (push+deep) * nx * (mass2/M);
            object_1.position.y += (push+deep) * ny * (mass2/M);
            object_2.position.x -= (push+deep) * nx * (mass1/M);
            object_2.position.y -= (push+deep) * ny * (mass1/M);

            //изменение скорости
            // TODO: чтобы использовать диссипацию, надо решить уравнения заново
            speed1.x = v1tx + ((mass1 - mass2) * v1nx + 2*mass2 * v2nx) / M;
            speed1.y = v1ty + ((mass1 - mass2) * v1ny + 2*mass2 * v2ny) / M;

            speed2.x = v2tx + ((mass2 - mass1) * v2nx + 2*mass1 * v1nx) / M;
            speed2.y = v2ty + ((mass2 - mass1) * v2ny + 2*mass1 * v1ny) / M;
        }
//
//        console.log('after  :\n' + 'o1:{' + speed1.x + ', ' +  speed1.y +'}\n'
//            + 'o2:{' + speed2.x + ', ' +  speed2.y +'}\n');
    },

    // range - расстояние передается для оптимизации (чтобы не вычислять лишний раз)
    checkCollision: function(object_1, object_2, precomputed_range){
        //TODO: убрать условия и сделать хеш-массив для выбора поведения
        if (object_1.shape=='Circle' && object_2.shape=='Circle') {
            if (precomputed_range < object_1.radius + object_2.radius) {
// !!!important!!! Это опитимизация для избавления от корня при рассчете колизий
//            var diffsq = object_1.radius + object_2.radius
//            if (precomputed_range < diffsq*diffsq) {
                // поведение, которое невозможно рассчитать у одного объекта
                if (object_1.collide(object_2) == 'bounce' || object_2.collide(object_1) == 'bounce') {
                    this.bounceCircles(object_1, object_2);
                    return true
                }
            }
        }
        return false
    },

    getRange: function(range_x, range_y){
        return Math.sqrt(range_x*range_x + range_y*range_y)
    },

    getRangeSq: function(range_x, range_y){
        return range_x*range_x + range_y*range_y
    },

    objectInField: function(object){
        if(object.position.x < 0 || object.position.x > this.fieldSize.width
            || object.position.y < 0 || object.position.y > this.fieldSize.height){
            object.destroy();
            return false;
        } else {
            return true;
        }
    },


    computeGravityAndCollisions: function(objects, timediff){
        var G = this.G,
            len = objects.length,
            getRange = this.getRange,
            getRangeSq = this.getRangeSq
//            checkCollision = this.checkCollision
//            objectInField = this.objectInField.call(this)
//            ,epsilon = 1;//минимальное расстояние для рассчета гравитации
            ;
        for (var i = 0; i < len; i++) {
            var current_object = objects[i];
            var gravity_impact_x=0, gravity_impact_y=0;
            if (this.objectInField(current_object)){
                for (var j = 0; j < len; j++) {
                    if (j!=i){
                        var impacting_object = objects[j];
                        var range_x = impacting_object.position.x - current_object.position.x,
                            range_y = impacting_object.position.y - current_object.position.y,
                            range = getRange(range_x, range_y);

                            // !!!important!!! Это опитимизация для избавления от корня при рассчете колизий

//                            range = getRangeSq(range_x, range_y);
                        if (!this.checkCollision(current_object, impacting_object, range)
                            && impacting_object.is_gravitating) {
//                            range = range < epsilon ? epsilon : range;
                            gravity_impact_x += impacting_object.mass / (range * range * range) * range_x;
                            gravity_impact_y += impacting_object.mass / (range * range * range) * range_y;
//                            var grav_base = impacting_object.mass / Math.pow(range, 1.5)
//                            gravity_impact_x += grav_base * range_x;
//                            gravity_impact_y += grav_base * range_y;
                        }
                    }
                }
                if (!current_object.is_static) {
                    current_object.speed.x += timediff * G * gravity_impact_x / 1000;
                    current_object.speed.y += timediff * G * gravity_impact_y / 1000;
                }
            }
        }
        return objects
    },

//    findCollisions:function(objects, timediff){
//        var len = objects.length;
//        for (var i = 0; i < len; i++) {
//            for (var j = 0; j<len; j++) {
//                if (j!=i){
//                    this.checkCollision(objects[i], objects[j]);
//                }
//            }
//        }
//        return objects;
//    },

    applyPhysicsStep:function(objects, timediff){
        var len = objects.length;
        this.objects.checkDead();
        for (var i=0; i < len; i++ ) {
            var current_object = objects[i];
            if (!current_object.is_static) {
                current_object.moveBySpeed(timediff);
            }
            current_object.clearCollisions();
            if (typeof (current_object.step) === 'function') {
//                console.log(2)
                current_object.step(timediff);
            }
        }
    },


    computePhysicsState: function(timediff){//step of the game
        // если использовать тему с лагом, тогда начинает графика тормозить когда физика тормозит
        // если нет, то ухудшается просчет физики и появляется глюк при сворачивании - разворачивании

        var lag = 20;//миллисекунды после которых считаем, что пошел лаг
        if (timediff > this.iterationTimeout + lag)  {timediff = lag;}

        var objects = this.objects;
//        objects = this.findCollisions(objects, timediff)
        objects = this.computeGravityAndCollisions(objects, timediff);

        // после просчета, проходим циклом еще раз, чтобы удалить уничтоженные/передвинуть активные
        this.applyPhysicsStep(objects, timediff)
    }
});
