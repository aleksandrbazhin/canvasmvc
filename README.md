# This MVC framework for html canvas and more #

### Roadmap ###

* Views update on model change
* DOM Views
* Collections
* Model hierarchy
* REST synchronization
* Drag and Drop and universal events for canvas and DOM
* Line Canvas View