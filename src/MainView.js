// это - вид, содержаший все канвасы, управляет рендером


fr.core.add('MainView', {
    init:function(){
        this.views = {}
    },
    addView : function(view){
        this.views[view.name] = view;
        return view;
    },

    startWaiting : function(){
        for (var viewName in this.views) {
            this.views[viewName].render();
            this.views.startWaiting();
        }
    },

    startAnimation: function(){
        this.previous_step_time = Date.now();
        requestAnimationFrame(this.animate(this));
    },

    animate: function(_this){
        return function(timestamp){
            _this.renderFrame(timestamp - _this.previous_step_time);
            _this.previous_step_time = timestamp;
            requestAnimationFrame(_this.animate(_this));
        }
    },

    renderFrame: function(timediff){ // step of the game
        for (var viewName in this.views) {
            this.views[viewName].render(timediff)
        }
    }
});