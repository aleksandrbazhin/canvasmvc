
// нужно бы и radius с position добавлять на атрибуты для CircleView или нет?
// не надо же мне их синхронизировать с сервером в самом деле
// возможно, стоит у атрибутов ставить флажок (синхронизируеимый/нет)
fr.CircleModel = fr.ObjectModel.extend({
//    modelName : 'CircleModel',
    shape : 'Circle',

    radius : 1,
    position: {x:0, y:0},

    // обязательно дл всех Shapes
    pointBelongs: function(x,y){
        var position = this.get('position'),
            radius = this.get('radius');
        diffx = position.x - x;
        diffy = position.y - y;
        if (radius * radius > diffx * diffx + diffy * diffy){
            return true;
        }
        return false;
    }
});