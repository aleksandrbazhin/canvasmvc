fr.core.add('control', {
    init : function(){
    },

    /*
    * observers - {
    *   view : canvasView
    *   event : string
    *   handler : function
    * }
    *
    * */
    addObservers : function(observers) {
        for (var observer in observers){
            observers[observer].view.getObservableElement().addEventListener(
                observers[observer].name,
                observers[observer].handler
            );
        }
    }

});