fr.Relations = {
    applyRelation : function(ownerObject, relation) {
        if (ownerObject[relation]) {
            console.log('duplicate property in schema and object definition');
        } else {
            return this[relation.type](ownerObject, relation.model, relation.url);
        }
    },
    hasMany : function (ownerObject, relatedModelName, url) {
//        console.log(this)
        var collection = new fr.ObjectCollection(fr.userModels[relatedModelName], url || relatedModelName + 's');
        // TODO: Нормальное удаление ссылок на объекты при удалении добавленных в листенере
//        console.log(ownerObject)
        collection.listen(ownerObject, 'fetch', collection.fetch, collection);
        return collection

    },

    hasOne : function (ownerObject, relatedModelName, url) {
        var model = new fr.userModels[relatedModelName];
        model.listen(ownerObject, 'fetch', collection.fetch, collection);
        return model;
    }

};