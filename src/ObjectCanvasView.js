fr.CanvasObjectView = fr.ObjectView.extend({


    init : function (model, transform) {
        this.model = model;
        // позиция на канвасе
        this.position = transform(this.model.get('position'));
//        if (this.model.hasOwnProperty('view_attribu   tes')) {
//            fr.helpers.objectExtend( this, model.view_attributes);
////        delete model.view_attributes;
//        }
        this.initOwnCanvas();
    },

    initOwnCanvas : function() {
        this.ownCanvas = window.document.createElement('canvas');
        this.ownCanvasContext = this.ownCanvas.getContext('2d');
    },
//
//    getTopLeftPosition : function(){
//        return {x: this.position.x - this.canvasCenter.x, y: this.position.y - this.canvasCenter.y};
//    },

    //установление позиции на канвасе
    updatePosition : function(transform){
        var new_position = transform(this.model.get('position'));
        if (!fr.helpers.compare(this.position, new_position)) {
            this.position = new_position;
            return true; // поменялась позиция
        }
        return false; // не поменялась
    },

    putOnCanvas :  function(canvas_context, scale) {
        canvas_context.drawImage(this.ownCanvas,
            this.position.x - this.canvasCenter.x * scale,
            this.position.y - this.canvasCenter.y * scale,
            this.ownCanvas.width * scale,
            this.ownCanvas.height * scale
        );
//        this.trigger('step', [this.position, this.model.get('color')]);
    }
});
