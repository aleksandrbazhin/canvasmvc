fr.core.add('ObjectCollection', {
    mixins : ['eventSource','eventListener'],
    init : function(model, name) {
//        console.log('collection created', url);
//        if (model) {
//        }
        this.name = name;
    },
//    fetch : function() {
//        console.log('fetching collection from ' + this.url);
//    },
    push : function(obj) {
        this.parent.push.call(this, obj);
        this.trigger('change');
        this.listen(obj, 'destroy', this._remove)
    },
    // служебный метод
    _remove : function(obj) {
        this.splice(this.indexOf(obj),1);
        this.trigger('change')
    },
    remove : function(obj) {
        obj.destroy();
    },
    // this is for the case of dealyed deleting
    checkDead : function() {
        var len = this.length, changed = false;
        for (var i=0; i < len; i++ ) {
            var current_object = this[i];
            if (current_object.marked_as_dead) { // удаление ссылки на уничтоженный
                current_object.trigger('destroy');
                this.splice(i,1);
                i--;
                len--;
                changed = true
            }
        }
        if (changed) {
            this.trigger('change');
        }
    },
}, Array);