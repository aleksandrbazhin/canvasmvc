// Может стоит замутить с большим количеством слоев?
// чтобы перерисовывались только при изменении принадлежащих им моделей
// соответственно,


fr.core.add('canvasLayer', {

    drawn : false,
    static : false,

    init: function(domId, width, height, zIndex){
        this.modelTypes = {};
        this.objectViews = [];
        var canvas = document.createElement('canvas');
        canvas.id = domId;
        document.body.appendChild(canvas);
        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);
        canvas.style.zIndex = zIndex;
        this.canvas = canvas;
        this.canvasContext = canvas.getContext('2d');
    },

    // transform - преобразование координат, передающееся с CanvasView
    render : function(transform, scale) {
//        if (this.objectViews && (!this.static || !this.drawn)) {
        this._checkObjectViews(transform);
        if (!this.drawn) {
            this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this._renderObjectViews(transform, scale);
            this.drawn = true;
        }
    },

    // modifies this.drawn and this.objectViews
    _checkObjectViews : function(transform) {
        if (!this.drawn) return;
        var objectViews = this.objectViews;
        var len = objectViews.length;
        for (var i=0; i < len; i++ ) {
            if (objectViews[i].marked_as_dead) {
                objectViews.splice(i, 1);
                i--;
                len--;
                this.drawn = false;
            } else {
                // вычисляем позицию с учетом смещения канваса
                if (objectViews[i].updatePosition(transform) || objectViews[i].update()) {
                    this.drawn = false;
                }
            }
        }
    },
    _renderObjectViews: function (transform, scale) {
        var objectViews = this.objectViews;
        var len = objectViews.length;
        for (var i=0; i < len; i++ ) {
            objectViews[i].putOnCanvas(this.canvasContext, scale);
        }

    },

// можно дописать, чтобы трейсы ресайзились все-таки
    transformContent : function(transformFunction, transformation){
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this._renderObjectViews(transformFunction, transformation.scale);
        this.drawn = false;
    }
});




