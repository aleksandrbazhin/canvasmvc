fr.registerModel('subCircle', fr.CircleModel.extend({
    radius: 5,
    attributes : {
        'position' : {x:0, y:0},
        'color' : '#000'
    },
    moveTo : function(x, y) {
        this.update('position', {x:x, y:y})
    },
}));



fr.registerModel('mahCircle', fr.CircleModel.extend({
    attributes : {
        'position' : {x:0, y:0},
        'color' : '#000'
    },
    radius : 10,
    init : function() {
//        this.fetch();
    },
    schema : {
        subCircles : {
            type: 'hasMany',
            model: 'subCircle'
        },
    },
    moveTo : function(x, y) {
        this.update('position', {x:x, y:y})
    },
}));
var MahView = fr.CircleView.extend({
    static:true,
    init: function() {
        // чтобы работали переопределенные render,
        // render должен вызываться в контексте дочернего объекта,
        // там где определен или выше
        // поэтому нельзя вызывать в родительском прототипе
        this.render();
    },
});


var MahDOMObjectView = fr.DOMObjectView.extend({
    init : function() {
        // перерисовываем, если у модели изменились свойства из списка
        console.log('initDom')
        this.observeProperties(['position']);
        this.render();
    }
});

// главная модель с каким-то произвольным функционалом
var MahMainModel = fr.MainModel.extend({
    init : function(size){
        this.size = size;
    },
    startUpdate : function() {
        var iterationTimeout = 1000;
        this.previous_step_time = Date.now();
        this.intervalHandler = setInterval(this.iterate( this), iterationTimeout);
    },
    iterate: function(_this){
        return function() {
            _this.objects.forEach(
                function(obj) {
                    obj.moveTo( Math.random() * _this.size.width, Math.random() * _this.size.height );
                }
            );
//            for (var i in _this.objects) {
//                _this.objects[i].moveTo( Math.random() * _this.size.width, Math.random() * _this.size.height );
//            }

        }
    },
});

var MyCanvasView = fr.CanvasView.extend({
   continuusRedraw : false
});

var TestExample = fr.MainController.extend({
    init : function() {
        var size = {width: window.innerWidth, height: window.innerHeight};
        this.model = new MahMainModel(size);

        // создаем view
        var canvasView = new fr.CanvasView('MainCanvas', size.width, size.height);
        // добавляем модели, которые будут отображаться на вью и указываем, какие вью их отображают
        canvasView.addModelObserve('mahCircle', MahView);
        //можно сделать и addModelObserve('mahCircle', fr.CircleView);

        //приделываем главному вью конкретный новый канвасный вью
        this.view.addView(canvasView);

        var domView = new fr.DOMView('div', 'MainDiv');
        domView.addModelObserve('mahCircle', MahDOMObjectView );
        this.view.addView(domView);

        //важно добавлять объекты после объявления view где они наблюдаются
        this.addObjects([{
            "modelName" : "mahCircle", // название модели
            "color" : "#f00"
        },
        {
            "modelName" : "mahCircle", // название модели
            "color" : "#00f"
        }
        ]);

        this.model.startUpdate();

        this.view.startAnimation(); // перерисование всех view по requestAnimationFrame
    }
});