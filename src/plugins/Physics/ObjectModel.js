fr.helpers.objectExtend(fr.ObjectModel.prototype, {
    collisionStrategy: {},
    speed: {x:0, y:0},

    moveBySpeed : function(dt) {
        this.position.x += this.speed.x * dt;
        this.position.y += this.speed.y * dt;
    },
    collided:[],

    collide : function(obj) {
        var CollisionStrategies = {
            //this - делегат к столкнувшемуся объекту
            ignore : function(){},
            bounce : function(obj){this.bounce(obj);},
            destroy : function(obj){this.destroy(); }
        };
    //        console.log(obj)
    //        console.log(this.id)
        if (this.collided.indexOf(obj.id) == -1) {
            var action = this.collisionStrategy[obj.modelName];
            if (CollisionStrategies[action] !== undefined) {
                CollisionStrategies[action].call(this, obj);
            }
            this.collided.push(obj.id);
            return action;
        } else {
            return false;
        }
    },
    clearCollisions : function(){
        this.collided = [];
    }
});