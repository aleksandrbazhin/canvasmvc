fr.helpers = {
    // это чтобы не перетиралась ссылка на объект this.mainViewTransformation
    objectUpdate : function (object, properties) {
        for (var prop in properties) {
            if (properties.hasOwnProperty(prop) && object.hasOwnProperty(prop)){
                object[prop] = properties[prop];
            }
        }
    },

    // adds unexistent properties to target from source and sets them the values from source
    objectFill : function (target, source) {
        for (var prop in source) {
            if (!target.hasOwnProperty(prop) && source.hasOwnProperty(prop)){
                target[prop] = source[prop];
            }
        }
        return target;
    },

    objectExtend : function (target, source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
        return target;
    },

    objectClone : function (source) {
        var new_object = {};
        for (var a in source) {
            if (typeof(source[a]) == 'object') {
                new_object[a] = fr.helpers.objectClone(source[a])
            } else {
                new_object[a] = source[a];
            }
        }
        return new_object;
    },

    update : function(o1, o2) {
        if (typeof o1 == 'object' && typeof o2 == 'object') {
            fr.helpers.objectUpdate (o1, o2)
        } else {
            o1 = o2;
        }
    },

    compare : function (o1, o2) {
        if (typeof o1 == 'object' && typeof o2 == 'object') {
            // here the check is not full (one sided only for props in o1)
            for (var prop in o1) {
                if (o1.hasOwnProperty(prop) && o2.hasOwnProperty(prop) && !fr.helpers.compare(o1[prop], o2[prop])) {
                    return false;
                }
            }
            return true;
        } else {
            return o1 === o2;
        }

    },
}