fr.helpers.objectExtend(fr.CircleModel.prototype, {
    density: 1,
    is_static: false,
    is_gravitating: true,
    trace: false,
    collisionEnergyPreserve: 1,

    getVolume : function(radius) {
        return radius * radius * Math.PI; // S*density
    },
    getMass:function(){

        if (this.mass === undefined && this.radius !== undefined && this.density !== undefined )
            this.mass = this.density * this.getVolume(this.radius)
    },
    bounce: function(obj){
    },
    init:function(){
        this.getMass();
    }
});