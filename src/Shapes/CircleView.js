fr.CircleView = fr.CanvasObjectView.extend({
    init:function(){
        // а как он может быть defined?
        if (this.radius === undefined){
            this.radius = this.model.get('radius') || 1;
        }
        this.resetOwnCanvas();
        this.render();
    },
    findCanvasCenter : function(){
        this.canvasCenter = {x: this.ownCanvas.width/2|0, y: this.ownCanvas.height/2|0}
    },
    resetOwnCanvas : function() {
//        console.log(this.radius)
        this.ownCanvas.width = 2 * this.radius + 1;
        this.ownCanvas.height = 2 * this.radius + 1;
        this.findCanvasCenter();
    },
    clearOwnCanvas : function() {
        this.ownCanvasContext.clearRect(- this.canvasCenter.x, - this.canvasCenter.y, this.ownCanvas.width, this.ownCanvas.height);
    },

    render : function() {
        // тут просто рисование
        var own_context = this.ownCanvasContext;
        own_context.fillStyle = this.model.get('color');
        own_context.beginPath();
        own_context.arc(this.canvasCenter.x, this.canvasCenter.y , this.radius, 0, Math.PI*2, true);
        own_context.fill();
    },
});