fr.core.add('b', {
    a:1,
    init:function(){
        console.log('init1')
    }
});

var bv = fr.b.extend({
    c:3,
    init:function(){
        console.log('init2')
    }
});

var bf = bv.extend({
    e:5,
    init:function(){
        console.log('init3')
    }
});
var bh = bf.extend({
    f:6,
    init:function(){
        fr.helpers.objectExtend(this, arguments[0])
        console.log('init4')
    }

});

var a = new bh({d:4});
console.log(a, a.d, a.f, a.e, a.c, a.a);
//"init1"
//"init2"
//"init3"
//"init4"
//{parent: Object, d: 4} 4 6 5 3 1