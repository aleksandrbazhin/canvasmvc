// Может стоит замутить с большим количеством слоев?
// чтобы перерисовывались только при изменении принадлежащих им моделей
// соответственно,

// это канвасовый вид, может быть несколько слоев, каждый из которых - канвас
// в целом - канвас
// суть - наличие слоев


fr.core.add('CanvasView', {

    // надо как-то передавать отступы в канвас, а оттуда во все лэеры
    alignments : ['left', 'right', 'top', 'bottom'],
    transformation : {offsetX: 0, offsetY: 0, scale: 1.0},
    init: function(name, width, height, position, zIndex){

        this.layers = {};

        this.name = name;
        this.width = width;
        this.height = height;
        this.position = position || {left : 0, top : 0};
        this.zIndex = zIndex || 0;
        this._addLayer(name, undefined, this.zIndex);
    },

    //sets objectModel with @modelName to be displayed on @layerName by @objectView
    addModelObserve: function(modelName, objectView, layerName){
        this.layers[layerName || this.name].modelTypes[modelName] = objectView;
    },
    // может добавить image кроме color
    addBackground : function(layer, color){
        var bg = this._addLayer('background', layer, this.zIndex-1);
        bg.static = true;
        if (color !== undefined){
            bg.canvas.style.backgroundColor = color;
        }
    },

    // adds view of object to all layers it belongs to
    addObjectView : function(model) {
        var layers = this.layers;
        for (var layerName in layers) {
            if (layers[ layerName ].modelTypes.hasOwnProperty( model.modelName )) {
                var newViewObject = new layers[ layerName ].modelTypes[ model.modelName ]( model, this.makeTransformFunction());
                layers[layerName].objectViews.push( newViewObject );
            }
        }
    },

    // @layer is layerClass, which can be user-defined
    _addLayer : function(name, layer, zIndex) {
        var newName = name == this.name ? name : this.name + name;
        layer = new (layer || fr.canvasLayer)(newName, this.width, this.height, zIndex || 0);
        this._placeLayer(layer);
        return this.layers[name] = layer;
    },

    _placeLayer : function(layer){
        var canvas = layer.canvas
        canvas.style.position = "absolute";
        for (var alignment in this.alignments) {
            if (this.position[this.alignments[alignment]] !== undefined) {
                canvas.style[this.alignments[alignment]] = this.position[this.alignments[alignment]];
            }
        }
    },

    render : function() {
        for (var name in this.layers) {
            this.layers[name].render(this.makeTransformFunction(), this.transformation.scale);
        }
    },

    // identical transformation functions
    makeTransformFunction : function() {
        if (this.transfromFunction === undefined){
            this.transfromFunction = function(pos){return fr.helpers.objectClone(pos)};
        }
        return this.transfromFunction;
    },

    getObservableElement :  function() {
        return this.layers[this.name].canvas;
    }

});

