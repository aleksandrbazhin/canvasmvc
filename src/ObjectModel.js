fr.core.add('ObjectModel', {
//    modelName : 'BaseModel',
    mixins : ['eventSource'],
    init : function(attributes) {
        this.attributes = fr.helpers.objectClone(this.attributes);
        // присваиваем значения тем атрибутам, которые перданы в инит
        for (var a in attributes){
            if (this.attributes.hasOwnProperty(a)){
                this.attributes[a] = attributes[a];
            }
        }
//        this.attributes = attributes;
//        fr.helpers.objectExtend( this, attributes );
        this.id = fr.core.getUniqueId();
        this.applySchema(this.schema);

        if (attributes.hasOwnProperty('modelName')) {
            this.modelName = attributes.modelName;
            this.url = this.modelName;
        }
    },

    applySchema : function(schema) {
        for (relation in schema) {
            this[relation] = fr.Relations.applyRelation(this, schema[relation])
        }
    },

    destroy : function() {
        this.marked_as_dead = true;
        this.trigger('destroy', this);
// TODO:        this.clearEventListeners();
    },
    'get' : function(attribute) {
        if (this.attributes.hasOwnProperty(attribute)) {
            return this.attributes[attribute];
        } else if (attribute in this) { // если что-то не задано для каждого объекта,
                                        // возможно, оно задано на модели общим для всех объектов
            return this[attribute];
        }

    },
    'set' : function(attribute, value){
        this.attributes[attribute] = value;
        this.trigger('change');
    },
    // Обновляем атрибут модели, если он объект - частично (без уничтожения его)
    update : function(attribute, newValues) {
//        console.log(attribute, this.get(attribute))
        fr.helpers.update(this.get(attribute), newValues);
        this.trigger('change');
    },

//    fetch : function() {
////        console.log('fetching ' + this.modelName  + ' from ' + this.url);
//        this.trigger('fetch');
//    },

    toJSON : function() {
        return JSON.stringify(this.attributes);
    }

});