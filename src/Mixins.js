fr.mixins = {
    'eventSource' : {
//        events : {},
        initMixin : function() {
            this.events = {};
        },
        on : function(eventName, callback, context){
            if (this.events[eventName] === undefined){
                this.events[eventName] = [];
            }
            this.events[eventName].push({
                'callback' : callback,
                'context' : context
            });
        },
        trigger : function(eventName, params){
            if (this.events.hasOwnProperty(eventName)) {
                var event = this.events[eventName];
                for (listener in event) {
                    event[listener].callback.call(event[listener].context, params);
                }
            }
        },
        off : function() {

        }
    },

    'eventListener' : {
        // смысл написанного здесь - утерян
//        initMixin : function(){
//            // if object has model attribute, attach events from it to given handlers
//            if (this.model && this.model.events) {
//                for (var eventName in this.eventsCallbacks){
//                    if (this.eventsCallbacks.hasOwnProperty(eventName)){
//                        this.model.on(eventName, this[this.eventsCallbacks[eventName]], this)
//                    }
//                }
//            }
//        },
        listen : function(source, eventName, callback, context){
            source.on(eventName, callback, context || this)
        },
        listenAll : function(sourceModel, eventName, callback, context){
            var oldInit = sourceModel.prototype.init
            sourceModel.prototype.init = function() {
                oldInit.apply(this, arguments);
                this.on(eventName, callback, context || this);
            }
        },
        unbind :function(object){

        }
    }
}