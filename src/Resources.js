fr.core.add('Resources', {
    data : [],
    init : function(){

    },
    loadedCount : 0,
    processFunctions : {
        'image' : function(resource, callback, context) {
            var image = new Image();
            image.src = resource.url;
            resource.output = image;
            image.onload = function(){
                callback.call(context)
            }
        },
        'json' : function(resource, callback, context) {
            var oReq = new XMLHttpRequest();
            oReq.onreadystatechange = function(){
                if (oReq.readyState == 4) {
                    if (oReq.status == 200) {
                        resource.output = JSON.parse(oReq.responseText);
                        callback.call(context);
                    } else {
                        alert('load failed');
                    }
                }
            };
            oReq.open("GET", resource.url, true);
            oReq.send()
        }
    },

    // @resourcesList = [ {
    //     name : name,
    //     url : url,
    //     type : type
    // }, ...]
    addList : function(resourcesList){
        for (var i=0; i < resourcesList.length; i++) {
            this.data.push(resourcesList[i]);
        }
    },

    'get' : function(name) {
        for (var i=0; i < this.data.length; i++) {
            if (this.data[i].name === name) {
                return this.data[i].output;
            }
        }
        return false;
    },

    loadAll : function(callback, context){

        var i, resourcesCount = this.data.length,
            localCallback = function() {
                this.loadedCount++;
                if (this.loadedCount === resourcesCount){
                    callback.call(context)
                }
            };
        for (i = 0; i < resourcesCount; i++) {
            if (this.processFunctions.hasOwnProperty(this.data[i].type)) {
                this.processFunctions[this.data[i].type](this.data[i], localCallback, this);
            }
        }
    }
})