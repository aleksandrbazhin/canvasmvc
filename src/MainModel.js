fr.core.add('MainModel', {
    objects : (new fr.ObjectCollection()),
//    init : function {
//        this.objects = new fr.Collection();
//        this.objects.push({})
//    },

    addObject: function(model) {
        this.objects.push(model);
    },

    getModelsOfType: function(modelName) {
        return this.objects.filter(function(obj){return obj.modelName == modelName});
    },

    findObjectsByXY: function (x, y) {
        return this.objects.filter(function(obj){return obj.pointBelongs(x, y)});
    },

    start: function(field_size, objects){

    },


});