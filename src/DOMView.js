// TODO: вынести общие с канвасовым вью части в общего родителя (типа areaView)
fr.core.add('DOMView', {

    // надо как-то передавать отступы в канвас, а оттуда во все лэеры
    alignments : ['left', 'right', 'top', 'bottom'],

    init: function(el, domId, width, height, position, zIndex) {

        this.objectViews = [];
        this.modelTypes = {};
        this.name = domId;

        this.el = document.createElement(el);
        this.el.id = domId;

        document.body.appendChild(this.el);
        if (width) {
            this.el.style.width = width + 'px';
        }
        if (height) {
            this.el.style.height = height + 'px';
        }
        this.el.style.position = "absolute";
        this.el.style.zIndex = zIndex || 0;
        this.position = position || {left : 0, top : 0};
        for (var alignment in this.alignments) {
            if (this.position[this.alignments[alignment]] !== undefined) {
                this.el.style[this.alignments[alignment]] = this.position[this.alignments[alignment]];
            }
        }
        this.zIndex = zIndex || 0;
        this.drawn = false;
    },

    startWaitnig : function(){
//        this.object.on('change')
    },

    render : function() {
//        console.log(newContent)
        this._checkObjectViews();
        if (!this.drawn) {
            this.drawn = true;
            this.el.textContent = '';
            this.el.appendChild(this._renderObjectViews());
        }
    },
    // object view is just dom subviews
    addObjectView : function(model) {
        if (this.modelTypes.hasOwnProperty( model.modelName )) {
            var newViewObject = new this.modelTypes[ model.modelName ]( model );
            this.objectViews.push( newViewObject );
        }
    },

    // добавление, перерисовка вью измененившихся моделей и удаление связанных с удаленной моделью вьюх
    _checkObjectViews : function() {
        if (!this.drawn) return;
        var objectViews = this.objectViews,
            len = objectViews.length;
        for (var i = 0; i < len; i++) {
            // удаление
            if (objectViews[i].marked_as_dead) {
                objectViews.splice(i, 1);
                i--;
                len--;
                this.drawn = false;
            } else {
                var oldel = objectViews[i].el;
                if (objectViews[i].update()) {
                    // добавление
                    if (!this.el.contains(oldel)) {
                        this.el.appendChild(objectViews[i].el);
                    // перерисовка
                    } else {
                        this.el.replaceChild(objectViews[i].el, oldel);
                    }
                }
            }
        }
    },
//
//    // удалим дочерний
//    _remove : function(view){
//        console.log(obj, this.indexOf(obj))
//        this.splice(this.indexOf(obj)+1,1);
//        this.trigger('change')
//    },

    // returns documentFragment
    _renderObjectViews : function () {
        var docFragment = document.createDocumentFragment();
        for (var i = 0; i < this.objectViews.length; i++) {
            docFragment.appendChild(this.objectViews[i].el);
        }
        return docFragment;
    },

    addModelObserve: function(modelName, objectView) {
        this.modelTypes[modelName] = objectView;
    },

    getObservableElement :  function() {
        return this.el;
    },

});

