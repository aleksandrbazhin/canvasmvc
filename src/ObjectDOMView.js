fr.DOMObjectView = fr.ObjectView.extend({
    template_dummy : function(model){
        var docFrag = document.createDocumentFragment();
//        for (var i in model) {
//            console.log(model)
//            console.log(i, model.hasOwnProperty(i))
//            var el = document.createElement('div');
//            el.textContent = JSON.stringify(model[i]);
//            docFrag.appendChild(el);
//        }
        var el = document.createElement('div');
        el.textContent = JSON.stringify(model.toJSON());
        docFrag.appendChild(el);
        return docFrag;

    },


    init : function(model, el, template) {
        this.el = document.createElement(el || 'div');
        this.template = template || this.template_dummy;
        this.render();
    },

    getObservableElement :  function() {
        return this.el;
    },

    render : function() {
//        var new_docFrag = this.template(this.model);
//        if (this.el.parentNode) {
//            this.el.parentNode.replaceChild(this.el, new_docFrag);
//        } else {
//            this.el = new_docFrag;
//        }
        // of course this is bad
        // we need to update only changed parts of DOM
        this.el.textContent = '';
//        while(this.el.firstChild){
//            this.el.removeChild(this.el.firstChild);
//        }


        this.el.appendChild(this.template(this.model));
    },


});

